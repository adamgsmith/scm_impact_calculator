// api.js

const express = require('express');
const database = require('../database.js');
const bodyParser = require('body-parser');
const request = require('request-promise');
const xml = require('xml');
var route = express.Router();             // get an instance of the express Router
const parseString = require('xml2js').parseString;

var SCMFW = 'http://dellxp13-ags.int-link.com/SCMConnectorFramework/ConnectorFramework.asmx';

route
    .get('/Relationships', function(req, res) {
   
        if (req.query.urid) {
            database.query("SELECT urid,parent,child,impact,group_name FROM ISS_SCM_Relationship_Table WHERE urid = " + req.query.urid)
                .then(function (data) {
                    res.json({"result": data.rows[0]});
                })
                .catch(e => setImmediate(() => { throw e }))
        } else {
            database.query("SELECT urid,parent,child,impact,group_name FROM ISS_SCM_Relationship_Table")
                    .then(function (data) {
                        res.json({"result": data.rows});
                    })
                    .catch(e => setImmediate(() => { throw e }))
        }
    })
    .post('/Relationships/update', function(req, res) {
        
        database.query("UPDATE ISS_SCM_Relationship_Table SET impact = " + req.body.impact + ",group_name = '" + req.body.group_name + "' WHERE urid = " + req.body.urid) // Insert data
                .then(function (data) {
                    res.json({"result": data});
                })
                .catch(e => setImmediate(() => { throw e }))
    })
	.get('/cibyclass', function(req, res) {
		
		if (req.query.service) {
			console.log('Service ', req.query.service);
			var options = {
				method: 'GET',
				uri: SCMFW + '/GetServiceStructure?name=' + req.query.service +  '&levels=99&cmdbVersion=&queryType=1'
			};
		} else {
			var options = {
				method: 'GET',
				uri: SCMFW + '/GetServiceStructure?name=ALL&levels=99&cmdbVersion=&queryType=1'
			};
		}
		 
		request(options)
			.then(function (data) {
				parseString(data, function (err, result) {
					var temp = [];
					result.Response.Data[0].Records[0].Record.forEach(function (ele) {
						if (ele.group_name[0] === "undefined") {ele.group_name[0] = ""};
						temp.push({
							"urid": ele.urid[0],
							"group_name": ele.group_name[0],
							"parent": ele.parent[0],
							"child": ele.child[0],
							"impact": ele.impact[0]
						});
					})
					res.json(temp);
				});
			})
			.catch(function (err) {
				// Crawling failed...
			});
        
    })
    .post('/cibyclass/update', function(req, res) {
        
        database.query("UPDATE ISS_SCM_Relationship_Table SET impact = " + req.body.impact + ",group_name = '" + req.body.group_name + "' WHERE urid = " + req.body.urid) // Insert data
                .then(function (data) {
                    res.json({"result": data});
                })
                .catch(e => setImmediate(() => { throw e }))
    })
	.get('/classlist', function(req, res) {
            database.query("SELECT class_name FROM iss_scm_dataclass_table")
                .then(function (data) {
                    res.json({"result": data.rows});
                })
                .catch(e => setImmediate(() => { throw e }))
    })
    .post('/GroupImpact', function(req, res) {
       
        database.query("SELECT MAX(id) FROM iss_scm_impactrules_table") // Insert data
            .then(function (data) {

                database.query("DELETE FROM iss_scm_impactrules_table WHERE item = '" + req.body.parent + "'")
                        .then(function (data) {
                           
                        })
                        .catch(e => setImmediate(() => { throw e }))

               var max = data.rows[0].max;
                max = max + Math.floor(Math.random() * 10) + Math.floor(Math.random() * 10)  ;
                console.log('MAX ::  ', max);
                var query = "INSERT INTO iss_scm_impactrules_table VALUES(" + max + ",0,'" + req.body.parent + "','" + req.body.parent + "',2,100.000,100.000,0,'',100.000,'',33.334,'0 = 0','(any)','53807','')";
                database.query(query) // Insert data
                    .then(function (data) {
						
						console.log('INERTED RULE INTO TABLE', data);
						
						console.log('PARENT :: ', req.body.parent );
						
                        var options = {
                            method: 'POST',
                            uri: SCMFW + '/GenerateSubscriptionRules',
                            body: '<?xml version="1.0"?><service>'+req.body.parent+'</service>'
                        };

                        request(options)
                            .then(function (htmlString) {
                               console.log('1st REQUEST TO SCM FW');
                                request(options)
                                .then(function (htmlString) {
                                    
                                })
                                .catch(function (err) {
                                    // Crawling failed...
                                });
                            })
                            .catch(function (err) {
                                // Crawling failed...
                            });
                        
                        
                        res.json({"result": data});
                    })
                    .catch(e => setImmediate(() => { throw e }))
                    })
            .catch(e => setImmediate(() => { throw e }))

    })
    .get('/adam', function(req, res) {
        if (req.query.service) {
            var options = {
                method: 'POST',
                uri: SCMFW + '/GenerateSubscriptionRules',
                body: '<?xml version="1.0"?><service>'+req.query.service+'</service>'
            };

            request(options)
                .then(function (htmlString) {
                    res.set('Content-Type', 'text/xml');
                    res.send(xml(htmlString));
                })
                .catch(function (err) {
                    // Crawling failed...
                });
        }
    })

module.exports = route;

