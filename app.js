// app.js

// Grab the packages we need
const express    = require('express');        // call express
const app        = express();                 // define our app using express
const bodyParser = require('body-parser');
const exphbs  = require('express-handlebars');
const path = require('path');

// Grab the route files we need
const appRoutes = require('./routes/app');
const apiRoutes = require('./routes/api');

// Configure app to use bodyParser()
// This will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(function(req, res, next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
});

const port = process.env.PORT || 9090;        // set our port

// register the routes
// all of our routes will be prefixed with /api/v1
app.use('/', appRoutes);
app.use('/api/v1', apiRoutes)

// start the server
app.listen(port);
console.log('Magic happens on port ' + port);
