// database.js

const { Pool } = require('pg')
//const connectionString = 'postgresql://postgres:system1@opbridgedb01.int-link.com:5432/cmdb'
const connectionString = 'postgresql://postgres:system1@dellxp13-ags.int-link.com:5432/esl'

const pool = new Pool({
  connectionString: connectionString,
})

// Create the table forthe API
function createTable() {
  pool.query('CREATE TABLE nn_mobile_app_token (data JSON)') // run query in connection pool (creates JSON table)
    .then(res => console.log('table:', res)) // wait for it to finish and log
    .catch(e => setImmediate(() => { throw e })) // catch errors
}

// check if the table already exsits
//pool.query("SELECT EXISTS (SELECT 1 FROM pg_catalog.pg_class c JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace WHERE c.relname = 'nn_mobile_app_token' )")
//  .then(function (data) {
 ///   if (data.rows[0].exists) {
  //    console.log('Table exists');
  //  } else {
   //   createTable();
   // }
  //})
 /// .catch(e => setImmediate(() => { throw e }))

// Example query for querying JSON
// select * from nn__mobile_app_token where (data->>'type')::text = 'android';

// Example Syntax
//pool.query('SELECT * FROM iss_pp_alerts_table where state = 1')
//.then(res => console.log('alerts:', res.rows))
//.catch(e => setImmediate(() => { throw e }))

module.exports = pool;
